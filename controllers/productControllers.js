const mongoose = require("mongoose");
const Product = require("../models/product.js");

module.exports.addProduct = (productDetails, addProduct) => {
	if(addProduct.isAdmin == true){
		let newProduct = new Product({
		name: productDetails.name,
		description: productDetails.description,
		price: productDetails.price,
		stocks: productDetails.stocks
	})
	return newProduct.save().then((newProduct, error) => {
		if(error){
			return error;
		}
		else{
			return newProduct;
		}
	})
	}
	else{
		let message = Promise.resolve("Access to this functionality requires the user to have administrative privileges.");
		return message.then((value) => {return value});
	}	
	
}

module.exports.getAllProduct = () => {
    return Product.find({}).then(result => {
        return result;
    })
}

module.exports.getAllActiveProduct = () => {
    return Product.find({isActive: true}).then(result => {
        return result;
    })
}



module.exports.getSingleProduct = (productId) => {
	return Product.findById(productId).then(result => {
		return result;
	})
}


module.exports.updateProduct = (productId, data) => {
	if(data.isAdmin == true){
		return Product.findByIdAndUpdate(productId, 
			{
				name: data.product.name,
				description: data.product.description,
				price: data.product.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})

	}
	else{
		let message = Promise.resolve('To access this functionality, the user must have administrative privileges');
		return message.then((value) => {return value});
	}
}

module.exports.archiveProduct = (productId, data) => {
	if(data.isAdmin == true){
		return Product.findByIdAndUpdate(productId,
			{
				isActive: data.isActive
			}
		).then((result, error) =>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('Administrative privileges are required for accessing this functionality.');
		return message.then((value) => {return value});
	}
}
