const User = require("../models/user.js")
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Product = require("../models/product.js");
const product = require("../models/product.js");


//  "email": "philemonquinley@mail.com",
//  "password": "admin3000"
//  "email": "guestnotadmin@gmail",
//  "password": "GUSEter!no666",


module.exports.registerUser = (userDetails) => {
    let newUser = new User({
        firstName: userDetails.firstName,
        lastName: userDetails.lastName,
        email: userDetails.email,
        mobileNo: userDetails.mobileNo,
        password: bcrypt.hashSync(userDetails.password, 10)   

    })

    return newUser.save().then((user,error)=> {
        if(error){
            return false;
        }
        else{
            return true;
        }
    })
}

module.exports.loginUser = (req, res) =>{
	return User.findOne({email: req.body.email})
	.then(result => {
		if(result == null){
			return res.send(false);
		
		}
		else{
		
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
		
			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(result)});
			}
			else{
				return res.send(false);
			
			}
		}
	})
}

module.exports.retrieveUserDetails = (request, response) => {
 
    const userData = auth.decode(request.headers.authorization);

    console.log(userData)

    return User.findById(userData.id).then(result => {
        result.password = "******";
        response.send(result);
        
    })
}

// checkout feature

module.exports.checkoutProduct = async (request, response) => {

    const userData = auth.decode(request.headers.authorization)
    
    let productName = await Product.findById(request.body.productId).then(result => result.name);

    let price = await Product.findById(request.body.productId).then(result => result.price);
    console.log("This is my price", price)


    let newData = {

    
        userId: userData.id,
        email: userData.email,
        productId: request.body.productId,
        productName: productName,
        quantity: request.body.quantity,
        totalAmount : 0

    }
    newData.totalAmount = price*newData.quantity
    console.log(newData)

    let isUserUpdated = await User.findById(newData.userId)
    .then(user =>{
        user.order.push({
            purchasedOn: new Date(),
            products: [{
                productId: newData.productId,
                productName: newData.productName,
                quantity: newData.quantity,
                totalAmount : price*newData.totalAmount
            }]
            
        })
        return user.save()
        .then(result => {
            console.log(result);
            return true;
        })
        .catch(error => {
            console.log(error);
            return false;
        })
    })
    console.log(isUserUpdated);

    let isProductUpdated = await Product.findById(newData.productId)
    .then(product => {

        product.orders.push({
            userId: newData.userId,
            userEmail: newData.userEmail,
            quantity: newData.quantity
        })

        product.stocks = product.stocks - request.body.quantity;
        
        return product.save()
        .then(result =>{
            console.log(result);
            return true;
        })
        .catch(error => {
            console.log(error);
            return false;
        }) 
    })
    console.log(isProductUpdated);

    (isUserUpdated == true && isProductUpdated == true)? response.send(newData) : response.send(false);
}