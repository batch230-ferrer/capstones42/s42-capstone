const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js")


const app = express("./routes/userRoutes.js");

mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.ebpzzsg.mongodb.net/FerrerEcommerce?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to FerrerEcommerce-Mongo DB Atlas"));


app.use(cors());

app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes)
app.use("/products", productRoutes)

app.listen(process.env.PORT || 4000, () =>
    {console.log(`API is now online on port ${process.env.PORT || 4000}`)
});

