const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName : {
        type : String,
        required : [true, "First name is required"]
    },
    lastName : {
        type : String,
        required : [true, "Last name is required"]
    },
    email : {
        type : String,
        required : [true, "Email is required"]
    },
    password : {
        type : String,
        required : [true, "Password is required"]
    },
    mobileNo : {
        type : String,
        required : [true, "Mobile No is required"]
    },
    isAdmin : {
        type : Boolean,
        default : false
    },
    order : [
        {
           
        
            purchasedOn : {
                type : Date,
                default : new Date()
            },
            products : [
                {
                    productId : {
                        type: String,
                        required: [true, "ProductId is required"]
                    },
                    productName : {
                        type: String,
                        required: [true, "Product Name is required"]
                    },
                    quantity : {
                        type : Number,
                        default: 0
                    },
                    totalAmount : {
                        type: Number
                    }
                }
            ]
        }
    ]
})

module.exports = mongoose.model("User", userSchema);