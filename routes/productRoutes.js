const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js")
const auth = require("../auth.js");
const Product = require("../models/product.js");


router.post("/create", auth.verify, (request, response) => 
{
		const result = {
			product: request.body, 	
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}
	productControllers.addProduct(request.body, result).then(resultFromController => response.send(resultFromController));
})

router.get("/allProducts", (request, response) => {
    productControllers.getAllProduct().then(resultFromController => response.send(resultFromController))
})


router.get("/allActiveProduct", (request, response) => {
    productControllers.getAllActiveProduct().then(resultFromController => response.send(resultFromController))
})

router.get("/:productId", (request,response) => {
	productControllers.getSingleProduct(request.params.productId).then(resultFromController => response.send(resultFromController))
})

router.put("/:productId/update",  auth.verify, (request, response) =>
{
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	productControllers.updateProduct(request.params.productId, data).then(resultFromController => {
		response.send(resultFromController)
	})
})

router.put("/:productId/archive", (request, response) => {
	const data = {
		isActive: request.body.isActive,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	productControllers.archiveProduct(request.params.productId, data).then(resultFromController => {
		response.send(resultFromController)
	})
})





module.exports = router;