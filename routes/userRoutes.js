const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers.js")
const auth = require("../auth");


router.post("/register", (request, response) => {
    userControllers.registerUser(request.body).then(resultFromController => response.send(resultFromController));
})

router.post("/login", userControllers.loginUser);

router.get("/:userId/details", auth.verify, userControllers.retrieveUserDetails)

router.post("/checkout", auth.verify, userControllers.checkoutProduct)

module.exports = router;